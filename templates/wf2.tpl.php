<?php

/**
 * @file
 * Customize confirmation screen after successful submission.
 *
 * This file may be renamed "webform-confirmation-[nid].tpl.php" to target a
 * specific webform e-mail on your site. Or you can leave it
 * "webform-confirmation.tpl.php" to affect all webform confirmations on your
 * site.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $progressbar: The progress bar 100% filled (if configured). This may not
 *   print out anything if a progress bar is not enabled for this node.
 * - $confirmation_message: The confirmation message input by the webform
 *   author.
 * - $sid: The unique submission ID of this submission.
 * - $url: The URL of the form (or for in-block confirmations, the same page).

 *
 * For the find-your-edge template to work properly (i.e. populate results) the form keys of the
 * ruleset category needs to named accordingly.
 *
 * You will need to use these exact form keys (case sensitive):
 *  skills_identification_and_articulation_workshop
 *  career_development_course
 *  work_and_community_experiences
 *  pd_courses
 *  capstone_workshop
 *
 * Make sure that the correct number of results pulled is inputted into the
 * each field for each ruleset category. Work and Community Experiences must have a value
 * of 3 for the number of results. There is value less than 3, you will generate an
 * error on the results page.
 *
 * The form id for non-international students should be placed first,
 * followed by the one for international students.
 * For example: Form ID(s) = 1,2
 * Where 1 is the form id for non-international students and 2 is the form id for
 * international students.
 */

?>

<?php
/**
 * Helper function that checks if a string contains a number. Returns false
 * otherwise.
 *
 * @param $string
 *
 * @return bool
 */
  function has_number($string) {
    $len = drupal_strlen($string);
    for ($i = 0; $i < $len; $i++) {
      if(ctype_digit($string[$i])) {
        return TRUE;
      }
    }
    return FALSE;
  }

/**
 * Helper function to check if string is a course code
 * This assumes all course code will always start with a letter and end with either
 * a number or an uppercase letter. Furthermore, every course code will have at
 * least one number.
 *
 * @param $string
 *
 * @return bool
 */
  function is_course($string) {
    $first = $string[0];
    $last = $string[drupal_strlen($string) - 1];

    if (has_number($string) && ctype_alpha($first) && (ctype_digit($last) || ctype_upper($last))) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

/**
 * Helper function to check if a string is a PD course
 * This assumes that every PD course will start with "PD" and the last character
 * of the string is a number.
 *
 * @param $string
 *
 * @return bool
 */
  function is_pd_course($string) {
    $pd = $string[0] . $string[1];
    $last = $string[drupal_strlen($string) - 1];
    return (ctype_digit($last) && $pd == "PD");
  }

/**
 * Helper function to check if position is a Don position.
 *
 * @param $string
 *
 * @return bool
 */
  function is_don_position($string) {
    $length = drupal_strlen($string);
    $word = "";
    for ($i = 4; $i > 0; $i--) {
      $word .= $string[$length - $i];
    }
    return ($word == " Don" || $word == " don");
  }

/**
 * Helper function which filters the list into a list with only one Don position.
 *
 * @param $list
 *
 * @return array
 */
  function filter_don($list) {
    $length = count($list);

    for ($i = 0; $i < $length; $i++) {
      if (is_don_position($list[$i]->result)) {
        unset($list[$i]);
        // Reindex the keys in array.
        $list = array_values($list);
        $length--;
      }
    }
    return $list;
  }

/**
 * Checks if user has identified with a disability or declined to disclose
 * Will assume user is not a student with a disability until sees "YESACCESSA" or "YESACCESSB"
 *
 * @param $data
 * @param $rule
 *
 * @return bool
 */
function is_accessability($data) {
  $length = count($data);
  // Reindex array since page breaks will break the indexing sequence.
  $data = array_values($data);
  for ($i = 0; $i < $length; $i++) {
    if (isset($data[$i])) {
      $number_of_selections = count($data[$i]);
      for ($j = 0; $j < $number_of_selections; $j++) {
        if($data[$i][$j] == 'YESACCESSA') {
          return true;
        }
		elseif($data[$i][$j] == 'YESACCESSB') {
          return true;
        }
      }
    }
  }
  return false;
}

/**
 * Provides the start <a> tag if $string is not "No resources available".
 *
 * @param $string
 * @param $link
 */
  function gen_href_start($string, $link) {
    if ($string != "No resources available") {
      print '<a href="' . $link . '" target="_blank">';
    }
  }

/**
 * Provides the end <a> tag if $string is not "No resources available".
 *
 * @param $string
 */
  function gen_href_end($string) {
    if ($string != "No resources available") {
      print '</a>';
    }
  }

/**
 * Process the string so that it can properly added to a pdf template.
 *
 * @param $string
 *
 * @return mixed
 */
  function pdf_process($string) {
    $string = preg_replace('~<a href=\s*".*"\s*>~', '', $string);
    $string = preg_replace('~</a>~', '', $string);
    return $string;
  }

/**
 * After processing the results, update the database to hold the new results.
 * This allows the tokens to have the updated results so the FillPDFs using the tokens
 * will have the correct values.
 *
 * @param $node
 * @param $sid
 * @param $skill_name
 * @param $learn_skill
 * @param $pd_course
 * @param $practice_skill
 * @param $get_advice
 */
  function pdf_store_results($node, $sid, $skill_name, $learn_skill, $pd_course, $practice_skill, $get_advice) {

    // If for some reason the Webform is set up wrong, return immediately to avoid errors.
    if (!isset($skill_name['cid']) || !isset($learn_skill['cid']) ||
        !isset($practice_skill['cid']) || !isset($get_advice['cid']) || !isset($pd_course['cid'])) {
      return;
    }
    
  

    // Store component 1 info.
    foreach ($skill_name['rulesets'] as $index => $ruleset) {
      $skill_name['rulesets'][$index]['description'] = pdf_process($ruleset['description']);
    }
    db_update('conditional_rulesets_results')
      ->fields(array(
        'nid' => $node->nid,
        'sid' => $sid,
        'cid' => $skill_name['cid'],
        'rulesets' => json_encode($skill_name['rulesets'])
      ))
      ->condition('nid', $node->nid)
      ->condition('sid', $sid)
      ->condition('cid', $skill_name['cid'])
      ->execute();

    // Store component 2 info.
    foreach ($learn_skill['rulesets'] as $index => $ruleset) {
      $learn_skill['rulesets'][$index]['description'] = pdf_process($ruleset['description']);
    }
    db_update('conditional_rulesets_results')
      ->fields(array(
        'nid' => $node->nid,
        'sid' => $sid,
        'cid' => $learn_skill['cid'],
        'rulesets' => json_encode($learn_skill['rulesets'])
      ))
      ->condition('nid', $node->nid)
      ->condition('sid', $sid)
      ->condition('cid', $learn_skill['cid'])
      ->execute();

      // Store component pd_course info.

      foreach ($pd_course['rulesets'] as $index => $ruleset) {
          $pd_course ['rulesets'][$index]['description'] = pdf_process($ruleset['description']);
      }
      db_update('conditional_rulesets_results')
          ->fields(array(
              'nid' => $node->nid,
              'sid' => $sid,
              'cid' => $pd_course ['cid'],
              'rulesets' => json_encode($pd_course['rulesets'])
          ))
          ->condition('nid', $node->nid)
          ->condition('sid', $sid)
          ->condition('cid', $pd_course ['cid'])
          ->execute();


      // Store practice_skill info.
    foreach ($practice_skill['rulesets'] as $index => $ruleset) {
      $practice_skill['rulesets'][$index]['description'] = pdf_process($ruleset['description']);
    }
    db_update('conditional_rulesets_results')
      ->fields(array(
        'nid' => $node->nid,
        'sid' => $sid,
        'cid' => $practice_skill['cid'],
        'rulesets' => json_encode($practice_skill['rulesets'])
      ))
      ->condition('nid', $node->nid)
      ->condition('sid', $sid)
      ->condition('cid', $practice_skill['cid'])
      ->execute();

    // Store get_advice info.
    foreach ($get_advice['rulesets'] as $index => $ruleset) {
      $get_advice['rulesets'][$index]['description'] = pdf_process($ruleset['description']);
    }
    db_update('conditional_rulesets_results')
      ->fields(array(
        'nid' => $node->nid,
        'sid' => $sid,
        'cid' => $get_advice['cid'],
        'rulesets' => json_encode($get_advice['rulesets'])
      ))
      ->condition('nid', $node->nid)
      ->condition('sid', $sid)
      ->condition('cid', $get_advice['cid'])
      ->execute();
  }

/**
 * FIll the list of rulesets only if there is not enough to pull from.
 * If there is still enough to pull from after removing the default result,
 * then just remove the default ruleset.
 *
 * @param $node
 * @param $cid
 * @param $rulesets
 *
 * @return mixed
 */
  function find_your_edge_append_default_values($node, $cid, $rulesets) {
    // First find the default ruleset.
    $default = array();
    $default_index = 0;
    foreach ($rulesets['rulesets'] as $index => $ruleset) {
      if ($ruleset['default']) {
        $default = $ruleset;
        $default_index = $index;
        break;
      }
    }

    // Remove the default ruleset first.
    unset($rulesets['rulesets'][$default_index]);

    // If there is not enough to pull without default, fill with default rulesets.
    $number_of_rulesets = count($rulesets['rulesets']);
    if ($number_of_rulesets < $node->webform['components'][$cid]['extra']['number_of_results']) {
      $difference = $node->webform['components'][$cid]['extra']['number_of_results'] - $number_of_rulesets;
      for ($i = 0; $i < $difference; $i++) {
        $rulesets['rulesets'][] = $default;
      }
    }

    return $rulesets;
  }

/**
 * Obtain the list of rulesets for a given ruleset category.
 *
 * @param $node
 * @param $cid
 * @param $rulesets
 *
 * @return array
 */
  function get_component_results($node, $cid, $rulesets) {
    $results_list = array();
    $results_list['cid'] = $cid;

    // Determine if there is enough to pull.
    $rulesets = find_your_edge_append_default_values($node, $cid, $rulesets);

    // Start pulling values.
    for ($i = 0; $i < $node->webform['components'][$cid]['extra']['number_of_results']; $i++) {
      $element = pull_random_element($rulesets['rulesets']);
      $index = array_search($element, $rulesets['rulesets']);
      unset($rulesets['rulesets'][$index]);
      $results_list['rulesets'][] = $element;
    }
    return $results_list;
  }

/**
 * Similar to get_component_results except remove extra "Don" positions if necessary.
 *
 * @param $node
 * @param $cid
 * @param $rulesets
 *
 * @return array
 */
  function get_component_exp_results($node, $cid, $rulesets) {
    $results_list = array();
    $results_list['cid'] = $cid;

    // Determine if there is enough to pull.
    $rulesets = find_your_edge_append_default_values($node, $cid, $rulesets);

    // Start pulling values.
    for ($i = 0; $i < $node->webform['components'][$cid]['extra']['number_of_results']; $i++) {
      $element = pull_random_element($rulesets['rulesets']);
      $index = array_search($element, $rulesets['rulesets']);
      unset($rulesets['rulesets'][$index]);
      $results_list['rulesets'][] = $element;
      // If we've found a Don position, filter out the rest so we can't get another.
      // This should only be true at most ONCE.
      if (is_don_position($results_list['rulesets'][$i]['result'])) {
        $rulesets['rulesets'] = filter_don($results_list['rulesets']);
      }
    }

    return $results_list;
  }


/**
 * Checks if all the components have been properly populated with rulesets.
 * If not, fill with empty rulesets in order to avoid errors displaying, while also
 * indicating that something is wrong.
 *
 * @param $skill_name
 * @param $learn_skill
 * @param $pd_course
 * @param $practice_skill
 * @param $get_advice
 */
  function validate(&$skill_name, &$learn_skill, &$pd_course, &$practice_skill, &$get_advice) {
    $empty_ruleset = array(
      'result' => '',
      'description' => '',
      'url' => '',
    );
    $skill_name['rulesets'][0] = !isset($skill_name['rulesets'][0]) ? $empty_ruleset : $skill_name['rulesets'][0];
    $learn_skill['rulesets'][0] = !isset($learn_skill['rulesets'][0]) ? $empty_ruleset : $learn_skill['rulesets'][0];
    $pd_course['rulesets'][0] = !isset($pd_course['rulesets'][0]) ? $empty_ruleset : $pd_course['rulesets'][0];
    $practice_skill['rulesets'][0] = !isset($practice_skill['rulesets'][0]) ? $empty_ruleset : $practice_skill['rulesets'][0];
    $get_advice['rulesets'][0] = !isset($get_advice['rulesets'][0]) ? $empty_ruleset : $get_advice['rulesets'][0];
  }

  $submission = webform_get_submission($node->nid, $sid);
  $access_token = token_replace('[submission:access-token]', array('webform-submission' => $submission));

  $configurations = db_select('conditional_rulesets_configuration', 'crc')
    ->fields('crc')
    ->condition('nid', $node->nid)
    ->condition('template_file_name', 'wf2.tpl.php')
    ->execute()
    ->fetchAssoc();

  $unprocessed_results = db_select('conditional_rulesets_unprocessed_results', 'crpr')
    ->fields('crpr')
    ->condition('sid', $sid)
    ->condition('nid', $node->nid)
    ->execute()
    ->fetchAllAssoc('cid', PDO::FETCH_ASSOC);

  foreach ($unprocessed_results as $cid => $value) {
    $unprocessed_results[$cid]['rulesets'] = drupal_json_decode($unprocessed_results[$cid]['rulesets']);
  }
  unset($unprocessed_results['nid']);
  unset($unprocessed_results['sid']);
  

  $fids = explode(',', str_replace(' ', '', $configurations['fid']));
  $skill_name = array();
  $learn_skill = array();
  $pd_course = array();
  $practice_skill = array();
  $get_advice = array();

  foreach ($unprocessed_results as $cid => $info) {
    $form_key = $node->webform['components'][$cid]['form_key'];
    switch ($form_key) {
      case 'skill_name':
        $skill_name = get_component_results($node, $cid, $info);
        break;

      case 'learn_skill':
        $learn_skill = get_component_results($node, $cid, $info);
        break;

        case 'pd_course':
            $pd_course = get_component_results($node, $cid, $info);
            break;

      case 'practice_skill':
        $practice_skill = get_component_results($node, $cid, $info);
        break;

      case 'get_advice':
        $get_advice = get_component_results($node, $cid, $info);
        break;


    }
  }

  // Avoid offset errors in case webform is set up incorrectly.
  validate($skill_name, $learn_skill, $pd_course, $practice_skill, $get_advice);

  // Store results for PDF use.
  pdf_store_results($node, $sid, $skill_name, $learn_skill, $pd_course, $practice_skill, $get_advice);

  $accessability = false;
  if (is_accessability($submission->data)) {
    $accessability = true;
  }

  drupal_add_css(drupal_get_path('module', 'uw_conditional_rulesets') .
    '/conditional_rulesets_style.css', array('group' => CSS_DEFAULT, 'weight' => 99));
?>
<div class="flex-container">
    <div class="flex-message">
        <p>Here is a list of resources to help develop your selected employability skill. Each resource has a corresponding website, which can be accessed by clicking the black button next to the resource description.
        </p>
    </div>
    
    <div class="flex-message">
        <p>If you have any questions, contact <a href="mailto:ceca.accessability@uwaterloo.ca">Andrew Brunet</a>.
        </p>
        <hr>
    </div>


 
    <div class="call-to-action-top-wrapper">
      <?php gen_href_start($skill_name['rulesets'][0]['result'], $skill_name['rulesets'][0]['url']) ?>
      <div class="call-to-action-wrapper adjust-height">
        <div class="call-to-action-wrapper">
          <div class="edge-action-button-gray">
              <div class="call-to-action-big-text"> <?php print $skill_name['rulesets'][0]['result'] ?> </div>
            </div>
          </div>
        </div>
      <?php gen_href_end($skill_name['rulesets'][0]['result']) ?>
    </div>
    
   <div class="flex-component-title margin-top">
    <h2 class="edge-header">Learn the Skill</h2>
  </div>

  <div class="flex-component-block">
    <div class="component_square">
      <div class="call-to-action-top-wrapper">
        <?php gen_href_start($learn_skill['rulesets'][0]['result'], $learn_skill['rulesets'][0]['url']); ?>
          <div class="call-to-action-wrapper">
            <div class="call-to-action-theme-uWaterloo">
              <div class="call-to-action-big-text"> <?php print $learn_skill['rulesets'][0]['result']; ?> </div>
            </div>
          </div>
        <?php gen_href_end($learn_skill['rulesets'][0]['result']); ?>
      </div>
    </div>
  </div>

  <div class="flex-component-description">
    <div>
      <?php print $learn_skill['rulesets'][0]['description']; ?>
    </div>
  </div>
  
  
  <div class="flex-component-block">
    <div class="component_square">
      <div class="call-to-action-top-wrapper">
        <?php gen_href_start($learn_skill['rulesets'][1]['result'], $learn_skill['rulesets'][1]['url']); ?>
          <div class="call-to-action-wrapper">
            <div class="call-to-action-theme-uWaterloo">
              <div class="call-to-action-big-text"> <?php print $learn_skill['rulesets'][1]['result']; ?> </div>
            </div>
          </div>
        <?php gen_href_end($learn_skill['rulesets'][1]['result']); ?>
      </div>
    </div>
  </div>

  <div class="flex-component-description">
    <div>
      <?php print $learn_skill['rulesets'][1]['description']; ?>
    </div>
  </div>
  
  
  <div class="flex-component-block">
    <div class="component_square">
      <div class="call-to-action-top-wrapper">
        <?php gen_href_start($learn_skill['rulesets'][2]['result'], $learn_skill['rulesets'][2]['url']); ?>
          <div class="call-to-action-wrapper">
            <div class="call-to-action-theme-uWaterloo">
              <div class="call-to-action-big-text"> <?php print $learn_skill['rulesets'][2]['result']; ?> </div>
            </div>
          </div>
        <?php gen_href_end($learn_skill['rulesets'][2]['result']); ?>
      </div>
    </div>
  </div>

  <div class="flex-component-description">
    <div>
      <?php print $learn_skill['rulesets'][2]['description']; ?>
    </div>
  </div>

  <div id="pd-block" class="flex-component-block">
    <div class="component_square">
        <div class="call-to-action-top-wrapper">
            <?php gen_href_start($pd_course['rulesets'][0]['result'], $pd_course['rulesets'][0]['url']); ?>
            <div class="call-to-action-wrapper">
                <div class="call-to-action-theme-uWaterloo">
                    <div class="call-to-action-big-text"> <?php print $pd_course ['rulesets'][0]['result'] ?> </div>
                </div>
            </div>
            <?php gen_href_end($pd_course['rulesets'][0]['result']); ?>
        </div>
    </div>
</div>

<div id="pd-description" class="flex-component-description pd-block">
    <div>
        <?php print $pd_course ['rulesets'][0]['description'] ?>
    </div>
</div>

 <div class="flex-component-title margin-top">
    <h2 class="edge-header">Practice the Skill</h2>
  </div>
  
<div class="flex-component-block">
    <div class="component_square">
      <div class="call-to-action-top-wrapper">
        <?php gen_href_start($practice_skill['rulesets'][0]['result'], $practice_skill['rulesets'][0]['url']); ?>
        <div class="call-to-action-wrapper">
          <div class="call-to-action-theme-science">
            <div class="call-to-action-big-text"> <?php print $practice_skill['rulesets'][0]['result'] ?> </div>
          </div>
        </div>
        <?php gen_href_end($practice_skill['rulesets'][0]['result']); ?>
      </div>
    </div>
  </div>
  
 <div class="flex-component-description">
    <div>
      <?php print $practice_skill['rulesets'][0]['description'] ?>
    </div>
 </div>

 <div class="flex-component-block">
    <div class="component_square">
      <div class="call-to-action-top-wrapper">
        <?php gen_href_start($practice_skill['rulesets'][1]['result'], $practice_skill['rulesets'][1]['url']); ?>
        <div class="call-to-action-wrapper">
          <div class="call-to-action-theme-science">
            <div class="call-to-action-big-text"> <?php print $practice_skill['rulesets'][1]['result'] ?> </div>
          </div>
        </div>
        <?php gen_href_end($practice_skill['rulesets'][1]['result']); ?>
      </div>
    </div>
  </div>
  
    
<div class="flex-component-description">
    <div>
      <?php print $practice_skill['rulesets'][1]['description'] ?>
    </div>
</div>
  
  
  <div class="flex-component-title margin-top">
    <h2 class="edge-header">Get Advice</h2>
  </div>

  <div class="flex-component-block">
    <div class="component_square">
      <div class="call-to-action-top-wrapper">
        <?php gen_href_start($get_advice['rulesets'][0]['result'], $get_advice['rulesets'][0]['url']); ?>
          <div class="call-to-action-wrapper">
            <div class="call-to-action-theme-arts">
              <div class="call-to-action-big-text"> <?php print $get_advice['rulesets'][0]['result'] ?> </div>
            </div>
          </div>
        <?php gen_href_end($get_advice['rulesets'][0]['result']); ?>
      </div>
    </div>
  </div>

  <div class="flex-component-description">
    <div>
      <?php print $get_advice['rulesets'][0]['description'] ?>
    </div>
  </div>
  
  <?php
    if (isset($accessability) && $accessability) {
      print '<div id="pd-description-international" class="flex-component-description pd-block">';
    }
    else {
      print '<div id="pd-description" class="flex-component-description pd-block">';
    }
  ?>
    <div>
      <?php print $component_pd['rulesets'][0]['description'] ?>
    </div>
  </div>

  <div>
    <?php
      if (isset($accessability) && $accessability) {
          print '<p> AccessAbility Services offers co-op support if you have any permanent, temporary or suspected disabilities. Topics for discussion can include accommodations, developing a care and support plan and/or receiving referrals to additional on-campus services. Contact <a href="https://uwaterloo.ca/accessability-services/" target="_blank">AccessAbility Services</a> to learn more.</p>';
        }
      ?>
  </div>

<div class="flex-message margin-top">
    <p> View a
      <?php
		// Students who identify with disabilities
        if (isset($accessability) && $accessability) {
			print l("PDF version of your skills development planner.", fillpdf_pdf_link($form_id = $fids[1], null, $webform = array('nid'=>$node->nid,'sid'=>$sid)) . '&token=' . $access_token);
        }
		// Students without disabilities
		else {
			print l("PDF version of your skills development planner.", fillpdf_pdf_link($form_id = $fids[0], null, $webform = array('nid'=>$node->nid,'sid'=>$sid)) . '&token=' . $access_token);
        }
        ?>
    </p>
 </div>

 
    <div>
        <p class="caption"> <strong> *If you cannot find what you were looking for: </strong>
            click "refresh results" to generate new resources for your selected skill or 
                 start over the planner. </p>
    </div>

 <div class="flex-back-button-wrapper">
    <div id ="back-button" class="edge-action-button-wrapper adjust-height">
      <div class="call-to-action-wrapper">
        <?php
          $url = url('/node/' . $node->nid);
          print '<a href="' . $url . '">';
        ?>
          <div class="call-to-action-wrapper adjust-height">
            <div class="edge-action-button-gray">
              <div class="call-to-action-big-text">
                <?php print t("Start Over") ?>
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>
  </div>

  <div class="flex-redo-button-wrapper">
    <div id="redo-button" class="edge-action-button-wrapper alignment adjust-height">
      <div class="call-to-action-wrapper">
        <a id="redo-button-href" href="">
          <div id="redo-hover-area" class="call-to-action-wrapper adjust-height">
            <div class="edge-action-button-gray">
              <div class="call-to-action-big-text">
                <?php print t("Refresh Results") ?>
              </div>
            </div>
          </div>
        </a>
      </div>
    </div>
    <div class="text-box-hover-wrapper">
      <p class="text-box-hover">Refreshing results may yield new resources.
          Make sure to click on resources that interest you before refreshing.
      </p>
    </div>
  </div>

</div>