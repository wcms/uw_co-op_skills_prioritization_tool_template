<?php

/**
 * @file
 * Customize confirmation screen after successful submission.
 *
 * This file may be renamed "webform-confirmation-[nid].tpl.php" to target a
 * specific webform e-mail on your site. Or you can leave it
 * "webform-confirmation.tpl.php" to affect all webform confirmations on your
 * site.
 *
 * Available variables:
 * - $node: The node object for this webform.
 * - $progressbar: The progress bar 100% filled (if configured). This may not
 *   print out anything if a progress bar is not enabled for this node.
 * - $confirmation_message: The confirmation message input by the webform
 *   author.
 * - $sid: The unique submission ID of this submission.
 * - $url: The URL of the form (or for in-block confirmations, the same page).

 *
 * For the find-your-edge template to work properly (i.e. populate results) the form keys of the
 * ruleset category needs to named accordingly.
 *
 * You will need to use these exact form keys (case sensitive):
 *  skills_identification_and_articulation_workshop
 *  career_development_course
 *  work_and_community_experiences
 *  pd_courses
 *  capstone_workshop
 *
 * Make sure that the correct number of results pulled is inputted into the
 * each field for each ruleset category. Work and Community Experiences must have a value
 * of 3 for the number of results. There is value less than 3, you will generate an
 * error on the results page.
 *
 * The form id for non-international students should be placed first,
 * followed by the one for international students.
 * For example: Form ID(s) = 1,2
 * Where 1 is the form id for non-international students and 2 is the form id for
 * international students.
 */

?>

<?php
/**
 * Helper function that checks if a string contains a number. Returns false
 * otherwise.
 *
 * @param $string
 *
 * @return bool
 */
  function has_number($string) {
    $len = drupal_strlen($string);
    for ($i = 0; $i < $len; $i++) {
      if(ctype_digit($string[$i])) {
        return TRUE;
      }
    }
    return FALSE;
  }

/**
 * Helper function to check if string is a course code
 * This assumes all course code will always start with a letter and end with either
 * a number or an uppercase letter. Furthermore, every course code will have at
 * least one number.
 *
 * @param $string
 *
 * @return bool
 */
  function is_course($string) {
    $first = $string[0];
    $last = $string[drupal_strlen($string) - 1];

    if (has_number($string) && ctype_alpha($first) && (ctype_digit($last) || ctype_upper($last))) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

/**
 * Helper function to check if a string is a PD course
 * This assumes that every PD course will start with "PD" and the last character
 * of the string is a number.
 *
 * @param $string
 *
 * @return bool
 */
  function is_pd_course($string) {
    $pd = $string[0] . $string[1];
    $last = $string[drupal_strlen($string) - 1];
    return (ctype_digit($last) && $pd == "PD");
  }

/**
 * Helper function to check if position is a Don position.
 *
 * @param $string
 *
 * @return bool
 */
  function is_don_position($string) {
    $length = drupal_strlen($string);
    $word = "";
    for ($i = 4; $i > 0; $i--) {
      $word .= $string[$length - $i];
    }
    return ($word == " Don" || $word == " don");
  }

/**
 * Helper function which filters the list into a list with only one Don position.
 *
 * @param $list
 *
 * @return array
 */
  function filter_don($list) {
    $length = count($list);

    for ($i = 0; $i < $length; $i++) {
      if (is_don_position($list[$i]->result)) {
        unset($list[$i]);
        // Reindex the keys in array.
        $list = array_values($list);
        $length--;
      }
    }
    return $list;
  }

/**
 * Checks if user is an international student
 * Will assume non-international until sees "YESINTL"
 *
 * @param $data
 * @param $rule
 *
 * @return bool
 */
function is_international($data) {
  $length = count($data);
  // Reindex array since page breaks will break the indexing sequence.
  $data = array_values($data);
  for ($i = 0; $i < $length; $i++) {
    if (isset($data[$i])) {
      $number_of_selections = count($data[$i]);
      for ($j = 0; $j < $number_of_selections; $j++) {
        if($data[$i][$j] == 'YESINTL') {
          return true;
        }
      }
    }
  }
  return false;
}

/**
 * Provides the start <a> tag if $string is not "Other Experience".
 *
 * @param $string
 * @param $link
 */
  function gen_href_start($string, $link) {
    if ($string != "Other Experience") {
      print '<a href="' . $link . '" target="_blank">';
    }
  }

/**
 * Provides the end <a> tag if $string is not "Other Experience".
 *
 * @param $string
 */
  function gen_href_end($string) {
    if ($string != "Other Experience") {
      print '</a>';
    }
  }

/**
 * Process the string so that it can properly added to a pdf template.
 *
 * @param $string
 *
 * @return mixed
 */
  function pdf_process($string) {
    $string = preg_replace('~<a href=\s*".*"\s*>~', '', $string);
    $string = preg_replace('~</a>~', '', $string);
    return $string;
  }

  function pdf_store_results($node, $sid, $highinterest, $mediuminterest, $lowinterest){
  // Store component 1 info.
      foreach ($highinterest['rulesets'] as $index => $ruleset) {
          $highinterest['rulesets'][$index]['description'] = pdf_process($ruleset['description']);
      }
      db_update('conditional_rulesets_results')
          ->fields(array(
              'nid' => $node->nid,
              'sid' => $sid,
              'cid' => $highinterest['cid'],
              'rulesets' => json_encode($highinterest['rulesets'])
          ))
          ->condition('nid', $node->nid)
          ->condition('sid', $sid)
          ->condition('cid', $highinterest['cid'])
          ->execute();

      // Store component 2 info.
      foreach ($mediuminterest['rulesets'] as $index => $ruleset) {
          $mediuminterest['rulesets'][$index]['description'] = pdf_process($ruleset['description']);
      }
      db_update('conditional_rulesets_results')
          ->fields(array(
              'nid' => $node->nid,
              'sid' => $sid,
              'cid' => $mediuminterest['cid'],
              'rulesets' => json_encode($mediuminterest['rulesets'])
          ))
          ->condition('nid', $node->nid)
          ->condition('sid', $sid)
          ->condition('cid', $mediuminterest['cid'])
          ->execute();

      // Store component 3 info
      foreach ($lowinterest['rulesets'] as $index => $ruleset) {
          $lowinterest['rulesets'][$index]['description'] = pdf_process($ruleset['description']);
      }
      db_update('conditional_rulesets_results')
          ->fields(array(
              'nid' => $node->nid,
              'sid' => $sid,
              'cid' => $lowinterest['cid'],
              'rulesets' => json_encode($lowinterest['rulesets'])
          ))
          ->condition('nid', $node->nid)
          ->condition('sid', $sid)
          ->condition('cid', $lowinterest['cid'])
          ->execute();
  }
  function find_your_edge_append_default_values($node, $cid, $rulesets) {
    // First find the default rulesets.
    $default = array();
    $default_index = 0;
    foreach ($rulesets['rulesets'] as $index => $ruleset) {
      if ($ruleset['default']) {
        $default = $ruleset;
        $default_index = $index;
        break;
      }
    }

    // Remove the default rulesets first
    unset($rulesets['rulesets'][$default_index]);
    return $rulesets;
  }

  function get_component_results($node, $cid, $rulesets) {
    $results_list = array();
    $results_list['cid'] = $cid;

    // Determine if there is enough to pull.
    $rulesets = find_your_edge_append_default_values($node, $cid, $rulesets);
    $results_list['rulesets'] = $rulesets['rulesets'];
    return $results_list;
  }
  

/**
 * Checks if all the components have been properly populated with rulesets.
 * If not, fill with empty rulesets in order to avoid errors displaying, while also
 * indicating that something is wrong.
 * @param $highinterest
 * @param $mediuminterest
 * @param $lowinterest
 */
  function validate(&$highinterest, &$mediuminterest, &$lowinterest) {
    $empty_ruleset = array(
      'result' => '',
      'description' => '',
      'url' => '',
    );
    $highinterest['rulesets'][0] = !isset($highinterest['rulesets'][0]) ? $empty_ruleset : $highinterest['rulesets'][0];
    $mediuminterest['rulesets'][0] = !isset($mediuminterest['rulesets'][0]) ? $empty_ruleset : $mediuminterest['rulesets'][0];
    $lowinterest['rulesets'][0] = !isset($lowinterest['rulesets'][0]) ? $empty_ruleset : $lowinterest['rulesets'][0];
  }

  $submission = webform_get_submission($node->nid, $sid);
  $access_token = token_replace('[submission:access-token]', array('webform-submission' => $submission));

  $configurations = db_select('conditional_rulesets_configuration', 'crc')
    ->fields('crc')
    ->condition('nid', $node->nid)
    ->condition('template_file_name', 'wf1.tpl.php')
    ->execute()
    ->fetchAssoc();

  $unprocessed_results = db_select('conditional_rulesets_unprocessed_results', 'crpr')
    ->fields('crpr')
    ->condition('sid', $sid)
    ->condition('nid', $node->nid)
    ->execute()
    ->fetchAllAssoc('cid', PDO::FETCH_ASSOC);

  foreach ($unprocessed_results as $cid => $value) {
    $unprocessed_results[$cid]['rulesets'] = json_decode($unprocessed_results[$cid]['rulesets'],true);
  }
  unset($unprocessed_results['nid']);
  unset($unprocessed_results['sid']);

  $fids = explode(',', str_replace(' ', '', $configurations['fid']));
  $highinterest = array();
  $mediuminterest = array();
  $lowinterest = array();

  foreach ($unprocessed_results as $cid => $info) {
    $form_key = $node->webform['components'][$cid]['form_key'];
    switch ($form_key) {
      case 'high_interest':
        $highinterest = get_component_results($node, $cid, $info);
        break;

      case 'medium_interest':
        $mediuminterest = get_component_results($node, $cid, $info);
        break;

      case 'low_interest':
        $lowinterest = get_component_results($node, $cid, $info);
        break;
    }

  }

  // Avoid offset errors in case webform is set up incorrectly.
  //validate($highinterest, $mediuminterest, $lowinterest);

  // Debug
  $debug['component1'] = $highinterest;
  $debug['component2'] = $mediuminterest;
  $debug['component3'] = $lowinterest;

  // Store results for PDF use.
  pdf_store_results($node, $sid, $highinterest, $mediuminterest, $lowinterest);

  $international = false;
  if (is_international($submission->data)) {
    $international = true;
  }

  drupal_add_css(drupal_get_path('module', 'uw_conditional_rulesets') .
    '/conditional_rulesets_style.css', array('group' => CSS_DEFAULT, 'weight' => 99));
?>
<div class="flex-container">

    <h1>Employability Skills Scorecard</h1>

    <div class="flex-message">
        <p> We have organized the employability skills based on your answers to the six scenarios. If any skills appear under High or Medium Priority to Develop, consider learning more about those skill(s) by clicking the black boxes below. As a next step, explore skill-building resources and opportunities with the <a href="https://uwaterloo.ca/co-operative-education/skills-development-planner" target="_blank">Skills Development Planner</a>.
        </p>
        <p>If you have any questions, contact <a href="mailto:ceca.accessability@uwaterloo.ca">Andrew Brunet</a>.
        </p>
    </div>
    
          <div class="flex-component-title margin-top">
        <h3 class="edge-header"><u>High Priority to Develop:</u></h3>
    </div>

        <?php
        $sizeofhighinterest = count($highinterest['rulesets']);
        for($i = 0; $i < $sizeofhighinterest; $i++) {
            print       '<div class="flex-component-block">
                            <div class="component_square">
                               <div class="call-to-action-top-wrapper">';
            gen_href_start($highinterest['rulesets'][$i]['result'], $highinterest['rulesets'][$i]['url']);
            print                   '<div class="call-to-action-wrapper">
                                            <div class="call-to-action-theme-uWaterloo">
                                                   <div class="call-to-action-big-text">';
            print $highinterest['rulesets'][$i]['result'];
            print '                                      </div>    
                                                   </div>
                                            </div>';
            gen_href_end($highinterest['rulesets'][$i]['result']);
            print '                  </div>
                               </div>
                          </div>';
            print       '<div class="flex-component-description">';
            print              $highinterest['rulesets'][$i]['description'];
            print '</div>';

        }
        ?>



    <div class="flex-component-title margin-top">
        <h3 class="edge-header"><u>Medium Priority to Develop:</u></h3>
    </div>

        <?php
        $sizeofmediuminterest = count($mediuminterest['rulesets']);
        for($i = 0; $i < $sizeofmediuminterest; $i++) {
            print '<div class="flex-component-block">
                      <div class="component_square">
                         <div class="call-to-action-top-wrapper">';
            gen_href_start($mediuminterest['rulesets'][$i]['result'], $mediuminterest['rulesets'][$i]['url']);
            print              '<div class="call-to-action-wrapper">
                                          <div class="call-to-action-theme-science">
                                                <div class="call-to-action-big-text">';
            print $mediuminterest['rulesets'][$i]['result'];
            print '                                  </div>
                                                </div>
                                          </div>';
            gen_href_end($mediuminterest['rulesets'][$i]['result']);
            print '               </div>
                            </div>
                     </div>';
            print'<div class="flex-component-description">';
            print           $mediuminterest['rulesets'][$i]['description'];
            print '</div>';

        }
        ?>
        
<div class="flex-component-title margin-top">
        <h3 class="edge-header"><u>Low Priority to Develop:</u></h3>
    </div>

        <?php
    $sizeoflowinterest = count($lowinterest['rulesets']);
        for($i = 0; $i < $sizeoflowinterest; $i++) {
            print ' <div class="flex-component-block">
                        <div class="component_square">
                           <div class="call-to-action-top-wrapper">';
            gen_href_start($lowinterest['rulesets'][$i]['result'], $lowinterest['rulesets'][$i]['url']);
            print                '<div class="call-to-action-wrapper">
                                        <div class="call-to-action-theme-arts">
                                             <div class="call-to-action-big-text">';
            print $lowinterest['rulesets'][$i]['result'];
            print '                                </div>
                                             </div>
                                        </div>';
            gen_href_end($lowinterest['rulesets'][$i]['result']);
            print               '</div>
                            </div>
                      </div>';
            print    '<div class="flex-component-description">';
            print              $lowinterest['rulesets'][$i]['description'];
            print'</div>';

        }
        ?>
        

<hr/>
    <?php
    if (isset($international) && $international) {
        print '<div id="pd-description-international" class="flex-component-description pd-block">';
    }
    else {
        print '<div id="pd-description" class="flex-component-description pd-block">';
    }
    ?>

    <div>
        <?php
        if (isset($international) && $international) {
            print '<p> If you\'re an international student, you may need to adjust your path
          through EDGE depending on your student visa and/or work permits. It\'s possible to complete
          EDGE with experiential learning courses and on-campus experiences that don\'t involve permits.
          For more information, contact the EDGE team at <a href="mailto:edge@uwaterloo.ca">edge@uwaterloo.ca</a>. </p>';
        }
        ?>
    </div>


<div class="flex-message margin-top">
    <p> View a
      <?php
      {
      print l("PDF version of your skills scorecard.", fillpdf_pdf_link($form_id = $fids[0], null, $webform = array('nid'=>$node->nid,'sid'=>$sid)) . '&token=' . $access_token);
      }
        ?>
    </p>
 </div>

    

    <div class="flex-back-button-wrapper">
        <div id ="back-button" class="edge-action-button-wrapper adjust-height">
            <div class="call-to-action-wrapper">
                <?php
                $url = url('/node/' . $node->nid);
                print '<a href="' . $url . '">';
                ?>
                <div class="call-to-action-wrapper adjust-height">
                    <div class="edge-action-button-gray">
                        <div class="call-to-action-big-text">
                            <?php print t("Restart the Tool") ?>
                        </div>
                    </div>
                </div>
                </a>
            </div>
        </div>
    </div>

</div>